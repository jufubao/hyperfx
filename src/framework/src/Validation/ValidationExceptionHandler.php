<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\Framework\Validation;

use Hyperf\Context\Context;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\Utils\Codec\Json;
use Hyperf\Validation\ValidationException;
use Hyperfx\Framework\Constants\ContextResponseConst;
use Hyperfx\Framework\Constants\ErrorCode;
use Hyperfx\Framework\Constants\ErrorCodeX;
use Hyperfx\Framework\Support\Response;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class ValidationExceptionHandler extends ExceptionHandler
{
    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        $this->stopPropagation();
        /** @var \Hyperf\Validation\ValidationException $throwable */
        $body = $throwable->validator->errors()->first();
        if (! $response->hasHeader('content-type')) {
            $response = $response->withAddedHeader('Content-Type', 'application/json')->withHeader('Server', 'Jufubao');
        }

        $json = Response::json(ErrorCodeX::INVALID_ARGUMENT, $body);

        // 阻止异常冒泡
        $this->stopPropagation();
        Context::set(ContextResponseConst::CTX_RESPONSE_STATUS_CODE, 400);
        $response = $response->withStatus(400)->withBody(new SwooleStream(Json::encode($json)));
        Context::set(ContextResponseConst::CTX_RESPONSE_BODY_SIZE, $response->getBody()->getSize());
        return $response;
    }

    public function isValid(Throwable $throwable): bool
    {
        return $throwable instanceof ValidationException;
    }
}
