<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\Framework\Constants;

use Hyperf\Constants\AbstractConstants;

class ContextResponseConst extends AbstractConstants
{
    public const CTX_REQUEST_BODY_SIZE = 'xrequest.body.size';
    public const CTX_RESPONSE_BODY_SIZE = 'xresponse.body.size';
    public const CTX_RESPONSE_BODY = 'xresponse.body';
    public const CTX_RESPONSE_STATUS_CODE = 'xresponse.status_code';
    public const CTX_RESPONSE_GRPC_STATUS = 'xresponse.grpc-status';
}
