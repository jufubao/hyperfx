<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\Framework\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;
use Hyperf\Grpc\StatusCode;

/**
 * @method static string getMessage(string $code, $translate = null)
 * @method static int getHttpCode(string $code, $translate = null)
 * @method static string getGrpcStatus(string $code, $translate = null)
 */
#[Constants]
class ErrorCodeX extends AbstractConstants
{
    /**
     * @Message("参数错误")
     * @HttpCode("400")
     * @GrpcStatus("3")
     */
    public const INVALID_ARGUMENT = 'InvalidArgument';

    /**
     * @Message("用户未授权")
     * @HttpCode("401")
     * @GrpcStatus("16")
     */
    public const UNAUTHENTICATED = 'Unauthorized';

    /**
     * @Message("需要登录")
     * @HttpCode("401")
     * @GrpcStatus("16")
     */
    public const NeedLogin = 'NeedLogin';

    /**
     * @Message("您没有此权限")
     * @HttpCode("403")
     * @GrpcStatus("7")
     */
    public const PERMISSION_DENIED = 'PermissionDenied';

    /**
     * @Message("记录不存在")
     * @HttpCode("404")
     * @GrpcStatus("5")
     */
    public const NOT_FOUND = 'NotFound';

    /**
     * @Message("记录已存在")
     * @HttpCode("409")
     * @GrpcStatus("6")
     */
    public const ALREADY_EXISTS = 'AlreadyExists';

    /**
     * @Message("Server Error！")
     * @HttpCode("500")
     * @GrpcStatus("13")
     */
    public const SERVER_ERROR = 'ServerError';

    /**
     * @Message("Unknown")
     * @HttpCode("500")
     * @GrpcStatus("13")
     */
    public const UNKNOWN = 'Unknown';
}
