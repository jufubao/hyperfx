<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\Framework\Constants;

use Hyperf\Constants\AbstractConstants;
use Hyperf\Constants\Annotation\Constants;

#[Constants]
class ErrorCode extends AbstractConstants
{
    /**
     * @Message("参数错误")
     */
    public const INVALID_ARGUMENT = 400;

    /**
     * @Message("用户未授权")
     */
    public const UNAUTHENTICATED = 401;

    /**
     * @Message("您没有此权限")
     */
    public const PERMISSION_DENIED = 403;

    /**
     * @Message("记录不存在")
     */
    public const NOT_FOUND = 404;

    /**
     * @Message("记录已存在")
     */
    public const ALREADY_EXISTS = 409;

    /**
     * @Message("Server Error！")
     */
    public const SERVER_ERROR = 500;
    
    public static $MAPPING = [
        self::INVALID_ARGUMENT => ErrorCodeX::INVALID_ARGUMENT,
        self::UNAUTHENTICATED => ErrorCodeX::UNAUTHENTICATED,
        self::PERMISSION_DENIED => ErrorCodeX::PERMISSION_DENIED,
        self::NOT_FOUND => ErrorCodeX::NOT_FOUND,
        self::ALREADY_EXISTS => ErrorCodeX::ALREADY_EXISTS,
        self::SERVER_ERROR => ErrorCodeX::SERVER_ERROR,
    ];
}
