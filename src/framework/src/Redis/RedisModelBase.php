<?php
declare(strict_types=1);

namespace Hyperfx\Framework\Redis;

use Hyperf\Redis\Redis;
use Hyperf\Redis\RedisFactory;
use Hyperf\Utils\ApplicationContext;

class RedisModelBase {

    // 实例
    protected string $instance = '';

    // key
    protected string $key = '';

    // client
    protected Redis $client;

    public function __construct()
    {
        $container = ApplicationContext::getContainer();
        $this->client = $container->get(RedisFactory::class)->get($this->instance);
        // 解析KEY
        $this->key = $this->parserKey();
    }

    /**
     * 解析key
     */
    private function parserKey(): string {
        $prefix = Config(sprintf('redis.%s.prefix', $this->instance), Config('app_name'));
        return sprintf(
            '%s:redis:%s',
            $prefix,
            $this->key
        );
    }

    public function getKey(): string {
        return $this->key;
    }

    /**
     * @param string $key
     * @param array $params
     * @return string
     * @throws \Exception
     */
    public function generateKey(string $key, array $params)
    {
        $config = $this->mapping[$key] ?? [];
        if (empty($config)) {
            throw new \Exception("{$key} does not exist");
        }

        foreach ($params as $key => $value) {
            if (!in_array($key, $config['params'])) {
                throw new \Exception("params key {$key} not exist");
            }
        }

        //there is no params on the define
        if (!isset($config["params"]) || count($config["params"]) === 0) {
            return $config['key'];
        }

        //resort
        $fillParams = [];
        foreach ($config["params"] as $paramKey) {
            $fillParams[$paramKey] = $params[$paramKey];
        }
        $str = vsprintf($config['key'], $fillParams);
        $trimPrefix = $config['trim_prefix'] ?? false;
        if (!$trimPrefix) {
            $str = $this->key . $str;
        }
        return $str;
    }
}