<?php

declare(strict_types=1);

namespace Hyperfx\Framework\Support;

use Hyperfx\Framework\Constants\ErrorCode;
use Hyperfx\Framework\Constants\ErrorCodeX;
use Hyperfx\Framework\Logger\Logx;
use stdClass;

class Response
{
    /**
     * 响应json.
     */
    public static function json(string|int $code = '', string $msg = 'success', array $data = [], array $extend = []): array
    {
        if (empty($code) || $code === 200 || 0 == strcmp((string) $code, 'OK')) {
            $data['request_id'] = Logx::getTraceId();
            return $data;
        }
        if (is_int($code) && $code > 0) {
            $code = ErrorCode::$MAPPING[$code] ?? ErrorCodeX::UNKNOWN;
        }
        $data['code'] = $code;
        $data['message'] = $msg;
        $data['request_id'] = Logx::getTraceId();
        !empty($extend) && ($data = array_merge($data, $extend));
        return $data;
    }
}
