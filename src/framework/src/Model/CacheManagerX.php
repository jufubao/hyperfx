<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\Framework\Model;

use DateInterval;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Database\Model\Collection;
use Hyperf\Database\Model\Model;
use Hyperf\DbConnection\Collector\TableCollector;
use Hyperf\ModelCache\CacheableInterface;
use Hyperf\ModelCache\Config;
use Hyperf\ModelCache\Handler\HandlerInterface;
use Hyperf\Redis\RedisFactory;
use Hyperf\Redis\RedisProxy;
use Hyperf\Utils\ApplicationContext;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

class CacheManagerX
{
    /**
     * @var HandlerInterface[]
     */
    protected array $handlers = [];

    protected LoggerInterface $logger;

    protected TableCollector $collector;

    public function __construct(protected ContainerInterface $container)
    {
        $this->logger = $container->get(StdoutLoggerInterface::class);
        $this->collector = $container->get(TableCollector::class);

        $config = $container->get(ConfigInterface::class);
        if (! $config->has('databases')) {
            throw new InvalidArgumentException('config databases is not exist!');
        }

        foreach ($config->get('databases') as $key => $item) {
            $_config = new Config($item['cache'] ?? [], $key);
            $this->handlers[$key] = [
                'config' => $_config,
                'handler' => ApplicationContext::getContainer()->get(RedisFactory::class)->get($_config->getPool())
            ];
        }
    }



    /**
     * Fetch a model from cache.
     * @param mixed $id
     */
    public function findIdsByUniqueKey(string $uniqueKey, array $values, string $class): array
    {
        /** @var Model $instance */
        $instance = new $class();

        $name = $instance->getConnectionName();

        $handler = $this->handlers[$name];

        /* @var $cache RedisProxy*/
        $cache = $handler['handler'];
        /* @var $cache RedisProxy*/
        $config = $handler['config'];

        $keys = [];
        foreach ($values as $value) {
            $keys[] = $this->getCacheOneKey($uniqueKey, (string) $value, $instance, $config);
        }

        $data = $cache->mGet($keys);

        // 没有获取到的数据找到来
        $notValues = [];
        $existsIds = [];
        foreach ($data as $index => $reqItem) {
            if (false === $reqItem) {
                $notValues[] = $values[$index];
            } else {
                $existsIds[] = (int) $reqItem;
            }
        }
        // Fetch it from database, because it not exists in cache handler.
        if (!empty($notValues)) {
            $models = $instance->newQuery()->whereIn($uniqueKey, $notValues)->select(['id', $uniqueKey])->get();
            $ttl = $this->getCacheTTL($instance, $config);
            foreach ($models as $model) {
                $key = $this->getCacheOneKey($uniqueKey, (string) $model->$uniqueKey, $instance, $config);
                $cache->set($key, $model->id, $ttl);
                $existsIds[] = $model->id;
            }
            return $existsIds;
        }

        // It not exists in cache handler and database.
        return $existsIds;
    }

    /**
     * Fetch a model from cache.
     * @param mixed $id
     */
    public function findIdByUniqueKey(string $uniqueKey, mixed $value, string $class): int
    {
        /** @var Model $instance */
        $instance = new $class();

        $name = $instance->getConnectionName();

        $handler = $this->handlers[$name];

        /* @var $cache RedisProxy*/
        $cache = $handler['handler'];
        /* @var $cache RedisProxy*/
        $config = $handler['config'];

        $key = $this->getCacheOneKey($uniqueKey, (string) $value, $instance, $config);

        $data = $cache->get($key);
        if (!empty($data)) {
            return (int) $data;
        }

        $model = $instance->newQuery()->where($uniqueKey, $value)->select(['id', $uniqueKey])->first();
        if (empty($model)) {
            return 0;
        }
        $ttl = $this->getCacheTTL($instance, $config);

        $key = $this->getCacheOneKey($uniqueKey, (string) $model->$uniqueKey, $instance, $config);
        $cache->set($key, $model->id, $ttl);

        return $model->id;
    }

    /**
     * Fetch a model from cache.
     * @param mixed $id
     */
    public function setIdMapping(string $uniqueKey, mixed $value, int $id, $instance)
    {
        /** @var Model $instance */

        $name = $instance->getConnectionName();

        $handler = $this->handlers[$name];

        /* @var $cache RedisProxy*/
        $cache = $handler['handler'];
        /* @var $cache RedisProxy*/
        $config = $handler['config'];

        $key = $this->getCacheOneKey($uniqueKey, (string) $value, $instance, $config);
        
        $ttl = $this->getCacheTTL($instance, $config);
        $cache->set($key, $id, $ttl);
    }

    protected function getCacheTTL(Model $instance, Config $config): DateInterval|int|null
    {
        if ($instance instanceof CacheableInterface) {
            $ttl =  $instance->getCacheTTL() ?? $config->getTtl();
        } else {
            $ttl = $config->getTtl();
        }
        if ($ttl === 0) {
            return null;
        }
        return $ttl;
    }

    /**
     * @param int|string $id
     */
    protected function getCacheOneKey(string $key, string $value, Model $model, Config $config): string
    {
        return sprintf(
            $config->getCacheKey(),
            $config->getPrefix(),
            $model->getTable(),
            sprintf('uniq-%s-mapping', $key),
            $value
        );
    }
}
