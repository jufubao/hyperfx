<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\Framework\Model;

use Hyperf\Database\Model\Events\Created;
use Hyperf\Database\Model\Model;
use Hyperf\Utils\ApplicationContext;

trait CacheableX
{
    
    public function created(Created $event) {
        if (!property_exists($this, 'uniqueIndexMapping') || empty($this->uniqueIndexMapping)) {
            return;
        }
        $container = ApplicationContext::getContainer();
        $manager = $container->get(CacheManagerX::class);
        
        foreach ($this->uniqueIndexMapping as $field) {
            $val = $this->getAttributeValue($field);
            if (!empty($val)) {
                $manager->setIdMapping($field, $val, $this->id, $this);
            }
        }
    }

    /**
     * 查询多个唯一索引字段的对应的ID
     *
     */
    public static function findIdsByUniqueKey(string $uniqueKey, array $items): array
    {
        $container = ApplicationContext::getContainer();
        $manager = $container->get(CacheManagerX::class);

        return $manager->findIdsByUniqueKey($uniqueKey, $items, static::class);
    }

    /**
     * 查询一个唯一索引字段的对应的ID
     *
     */
    public static function findIdByUniqueKey(string $uniqueKey, mixed $value): int
    {
        $container = ApplicationContext::getContainer();
        $manager = $container->get(CacheManagerX::class);

        return $manager->findIdByUniqueKey($uniqueKey, $value, static::class);
    }
}
