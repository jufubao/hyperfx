<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\Framework\Command;

use Hyperf\Command\Event;
use Hyperf\Contract\Arrayable;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Coroutine;
use Hyperf\Utils\Str;
use Hyperf\Validation\ValidatorFactory;
use Psr\EventDispatcher\EventDispatcherInterface;
use Swoole\ExitException;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableStyle;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

use Hyperf\Command\Command as HyperfCommand;

abstract class Command extends HyperfCommand
{
    protected int $exitCode = 1;

    protected function checkArguments(array $rules, array $messages = [],  array $customAttributes = []): bool|array {
        $options = $this->input->getOptions();
        $validator = ApplicationContext::getContainer()->get(ValidatorFactory::class)->make(
            $options,
            $rules,
            $messages,
            $customAttributes
        );
        if ($validator->fails()) {
            $errorMessage = $validator->errors()->first();
            $this->error($errorMessage);
            return false;
        }
        return $validator->validated();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->disableDispatcher($input);

        $callback = function () {
            try {
                $this->eventDispatcher && $this->eventDispatcher->dispatch(new Event\BeforeHandle($this));
                $exitCode = $this->handle();
                $this->eventDispatcher && $this->eventDispatcher->dispatch(new Event\AfterHandle($this));
            } catch (\Throwable $exception) {
                if (class_exists(ExitException::class) && $exception instanceof ExitException) {
                    return $this->exitCode = (int) $exception->getStatus();
                }

                if (! $this->eventDispatcher) {
                    throw $exception;
                }

                $this->output && $this->error($exception->getMessage());

                $this->eventDispatcher->dispatch(new Event\FailToHandle($this, $exception));
                $this->exitCode = $exception->getCode() ?: $this->exitCode;
                return $this->exitCode;
            } finally {
                $this->eventDispatcher && $this->eventDispatcher->dispatch(new Event\AfterExecute($this));
            }
            $this->exitCode = is_int($exitCode) ? $exitCode : 0;
            return $this->exitCode;
        };
        if ($this->coroutine && ! Coroutine::inCoroutine()) {
            run($callback, $this->hookFlags);
            return $this->exitCode;
        }
        return $callback();
    }
}