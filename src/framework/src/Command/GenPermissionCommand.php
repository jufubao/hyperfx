<?php

declare(strict_types=1);

namespace Hyperfx\Framework\Command;

use Hyperf\Command\Annotation\Command;
use Hyperf\Command\Command as HyperfCommand;
use Hyperf\HttpServer\Router\DispatcherFactory;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Codec\Json;
use Hyperf\Utils\CodeGen\Project;
use Hyperf\Utils\Composer;
use Hyperf\Utils\Str;
use Psr\Container\ContainerInterface;
use Swoole\ExitException;
use Symfony\Component\Console\Input\InputOption;

/**
 * 生成权限值文件
 */
#[Command]
class GenPermissionCommand extends HyperfCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct('gen:permission');
    }

    public function configure()
    {
        $this->setDescription('生成权限值文件');
    }

    private function parseHandler(\Hyperf\HttpServer\Router\Handler $handler, $method) {
        $callback = $handler->callback;
        if (is_array($callback)) {
            $arr = $callback;
        } else {
            $arr = explode('@', $callback);
        }
        $classObj = new \ReflectionClass($arr[0]);// 反射 TwoTestController 类
        $doc = $classObj->getMethod($arr[1])->getDocComment();
        // 解决swagger文档生成的
        preg_match('/summary="(.*+)/', $doc, $newArr);
        $name = $handler->options['handler_doc'] ?? $handler->options['handler'];
        if (!empty($newArr)) {
            var_dump($name);
        }
        $data = [
            'label' => sprintf('%s %s (%s)', $method, $name, $handler->route),
            'value' => $handler->options['handler']
        ];
        $group = str_replace('App\Controller\\', '', $arr[0]);
        $group = str_replace('Controller', '', $group);
        $group = str_replace('\\', '/', $group);
        return [$group, $data];
    }
    public function handle()
    {
        $routeData = ApplicationContext::getContainer()->get(DispatcherFactory::class)->getRouter('http')->getData();
        $group = [];
        foreach ($routeData as $routeItems) {
            foreach ($routeItems as $method => $routeItem) {
                /* @var $handler \Hyperf\HttpServer\Router\Handler*/
                foreach ($routeItem as $handler) {
                    if ($handler instanceof \Hyperf\HttpServer\Router\Handler) {
                        $pared = $this->parseHandler($handler, $method);
                        $group[$pared[0]][] = $pared[1];
                    } else if (is_array($handler) && isset($handler['routeMap'])) {
                        foreach ($handler['routeMap'] as $_routeItem) {
                            $handler = $_routeItem[0];
                            if (isset($handler->options['handler'])) {
                                $pared = $this->parseHandler($handler, $method);
                                $group[$pared[0]][] = $pared[1];
                            }
                        }
                    }
                }
            }
        }

        $output = [];
        foreach ($group  as $groupName => $items)  {
            $output[] = [
                'group' => $groupName,
                'items' => $items,
            ];
        }

        $content = Json::encode($output);
        $content = str_replace('\\', '', $content);
        file_put_contents(BASE_PATH . '/' . 'permission.json', $content);
        $this->line('生成成功', 'info');
    }
}