<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\Framework\Middleware;

use Google\Protobuf\Internal\Message as ProtobufMessage;
use Grpc\Base\Partner\V1\ListPartnerNamesResponse;
use Grpc\Mall\Product\V1\ListSelectedProductsRequest;
use Hyperf\Contract\ContainerInterface;
use Hyperf\Di\MethodDefinitionCollector;
use Hyperf\Di\ReflectionManager;
use Hyperf\Grpc\Parser;
use Hyperf\Server\Exception\ServerException;
use Hyperf\Utils\Network;
use Hyperfx\Framework\Constants\ContextResponseConst;
use Hyperfx\Framework\Logger\Logx;
use Hyperfx\Utils\NetworkUtil;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Hyperf\HttpServer\Router\Dispatched;
use Hyperf\Context\Context;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class CoreMiddleware implements MiddlewareInterface
{
    public function __construct(protected ContainerInterface $container)
    {
        
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        Context::set(ContextResponseConst::CTX_REQUEST_BODY_SIZE, $request->getBody()->getSize());
        $body = $this->getBody($request);
        Context::set(ContextResponseConst::CTX_RESPONSE_BODY, $body);
        $response =  $handler->handle($request);
        Context::set(ContextResponseConst::CTX_RESPONSE_BODY_SIZE, $response->getBody()->getSize());
        return $response;
    }

    private function getBody(ServerRequestInterface $request) {
        if ($request->getBody()->getSize() > 40960) {
            return '';
        }
        if (0 == strcmp($request->getHeaderLine('content-type'), 'application/grpc+proto')) {
            // grpc
            /** @var Dispatched $dispatched */
            $dispatched = $request->getAttribute(Dispatched::class);

            if (! $dispatched instanceof Dispatched) {
                throw new ServerException(sprintf('The dispatched object is not a %s object.', Dispatched::class));
            }
            [$controller, $action] = $this->prepareHandler($dispatched->handler->callback);
            $parameters = $this->parseMethodParameters($controller, $action, $dispatched->params);
            if (isset($parameters[0])) {
                $body = $parameters[0]->serializeToJsonString();
                if (str_contains($body, 'password"')) {
                    $body = preg_replace('/"password":"[\w\W]+"/', '"password":"******"',  $body);
                    $body = preg_replace('/"confirm_password":"[\w\W]+"/', '"confirm_password":"******"',  $body);
                    $body = preg_replace('/"old_password":"[\w\W]+"/', '"old_password":"******"',  $body);
                }
                if (str_contains($body, 'secret"')) {
                    $body = preg_replace('/"secret":"[\w\W]+"/', '"secret":"******"',  $body);
                }
                return $body;
            }
        }
	// http
        $body = $request->getParsedBody();
        if (is_array($body)) {
            isset($body['password']) && $body['password'] = '******';
            isset($body['confirm_password']) && $body['confirm_password'] = '******';
            isset($body['old_password']) && $body['old_password'] = '******';
            isset($body['secret']) && $body['secret'] = '******';
        }
        return $body;
    }

    /**
     * @return RequestInterface
     */
    protected function request()
    {
        return Context::get(ServerRequestInterface::class);
    }

    protected function parseMethodParameters(string $controller, string $action, array $arguments): array
    {
        $injections = [];
        $definitions = MethodDefinitionCollector::getOrParse($controller, $action);

        foreach ($definitions ?? [] as $definition) {
            if (! is_array($definition)) {
                throw new \RuntimeException('Invalid method definition.');
            }
            if (! isset($definition['type']) || ! isset($definition['name'])) {
                $injections[] = null;
                continue;
            }
            $injections[] = value(function () use ($definition) {
                switch ($definition['type']) {
                    case 'object':
                        $ref = $definition['ref'];
                        $class = ReflectionManager::reflectClass($ref);
                        $parentClass = $class->getParentClass();
                        if ($parentClass && $parentClass->getName() === ProtobufMessage::class) {
                            $request = $this->request();
                            $stream = $request->getBody();
                            return Parser::deserializeMessage([$class->getName(), null], (string) $stream);
                        }

                        if (! $this->container->has($definition['ref']) && ! $definition['allowsNull']) {
                            throw new \RuntimeException(sprintf('Argument %s invalid, object %s not found.', $definition['name'], $definition['ref']));
                        }

                        return $this->container->get($definition['ref']);
                    default:
                        throw new \RuntimeException('Invalid method definition detected.');
                }
            });
        }

        return $injections;
    }

    protected function prepareHandler($handler): array
    {
        if (is_string($handler)) {
            if (strpos($handler, '@') !== false) {
                return explode('@', $handler);
            }
            return explode('::', $handler);
        }
        if (is_array($handler) && isset($handler[0], $handler[1])) {
            return $handler;
        }
        throw new \RuntimeException('Handler not exist.');
    }
}
