<?php

declare(strict_types=1);

namespace Hyperfx\Framework\Exception;

use Grpc\Base\Idaas\V1\App;
use Hyperf\Grpc\StatusCode;
use Hyperf\Server\Exception\ServerException;
use Hyperfx\Framework\Constants\ErrorCode;
use Hyperfx\Framework\Constants\ErrorCodeX;
use Throwable;

class AppException extends ServerException
{
    /**
     * 上下文信息.
     *
     * @var array
     */
    protected $context = [];

    private bool $debug = true;

    public function __construct(int|string $code, string $message = null, array $context = [], Throwable $previous = null)
    {
        $this->context = $context;

        if (is_string($code)) {
            // 优先使用项目下的ErrorCodex
            if (class_exists('App\Constants\ErrorCodeX')) {
                if (is_null($message)) {
                    $message = \App\Constants\ErrorCodeX::getMessage($code);
                }
                $httpCode = (int) \App\Constants\ErrorCodeX::getHttpCode($code);
            } else {
                if (is_null($message)) {
                    $message = \Hyperfx\Framework\Constants\ErrorCodeX::getMessage($code);
                }
                $httpCode = (int) \Hyperfx\Framework\Constants\ErrorCodeX::getHttpCode($code);
            }
            $message = sprintf('%s##%s', $code, $message);
            $code = $httpCode;
        } else if (is_int($code)) {
            if (is_null($message)) {
                // 优先使用项目下的ErrorCode
                if (class_exists('App\Constants\ErrorCode')) {
                    $message = \App\Constants\ErrorCode::getMessage($code);
                } else {
                    $httpCode = $code > 100 ? $code : (StatusCode::HTTP_CODE_MAPPING[$code] ?? 500);
                    $message = \Hyperfx\Framework\Constants\ErrorCode::getMessage($httpCode);
                }
            }
        }
        
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return array
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @return bool
     */
    public function isDebug(): bool
    {
        return $this->debug;
    }

    /**
     * @param bool $debug
     */
    public function setDebug(bool $debug): void
    {
        $this->debug = $debug;
    }
}