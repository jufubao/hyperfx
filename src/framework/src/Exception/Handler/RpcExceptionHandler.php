<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\Framework\Exception\Handler;

use Hyperf\Context\Context;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\ExceptionHandler\Formatter\FormatterInterface;
use Hyperf\Grpc\StatusCode;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\Utils\Codec\Json;
use Hyperfx\Framework\Constants\ContextResponseConst;
use Hyperfx\Framework\Constants\ErrorCode;
use Hyperfx\Framework\Constants\ErrorCodeX;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class RpcExceptionHandler extends ExceptionHandler
{
    use BaseExceptionHandler;


    public function __construct(protected StdoutLoggerInterface $logger, protected FormatterInterface $formatter) {
    }

    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        $code = $this->geCode($throwable);

        $logLevel = $this->getLogLevel($throwable, $code);

        $this->logger->log($logLevel, $this->formatter->format($throwable));

        [$httpCode, $grpcStatus] = $this->handleCode($code, $throwable->getMessage());
        
        return $this->transferToResponse($httpCode, $grpcStatus, $throwable->getMessage(), $response);
    }

    public function isValid(Throwable $throwable): bool
    {
        return true;
    }

    private function handleCode(int $code, string $message): array {
        if (str_contains($message, '##')) {
            $arrMessage = explode('##', $message);
            $strCode = $arrMessage[0];
            if (class_exists('App\Constants\ErrorCodeX')) {
                $httpCode = (int) \App\Constants\ErrorCodeX::getHttpCode($strCode);
                $grpcCode = (int) \App\Constants\ErrorCodeX::getGrpcStatus($strCode);
            } else {
                $httpCode = (int) \Hyperfx\Framework\Constants\ErrorCodeX::getHttpCode($strCode);
                $grpcCode = (int) \Hyperfx\Framework\Constants\ErrorCodeX::getGrpcStatus($strCode);
            }
            return [$httpCode, $grpcCode];
        }
        if ($code < 1) {
            return [500, StatusCode::INTERNAL];
        }
        if ($code > 100) {
            // httpCode 情况
            $httpCode = $code;
            $strCode = ErrorCode::$MAPPING[$code] ?? ErrorCodeX::SERVER_ERROR; 
            if (class_exists('App\Constants\ErrorCodeX')) {
                $grpcCode = (int) \App\Constants\ErrorCodeX::getGrpcStatus($strCode);
            } else {
                $grpcCode = (int) \Hyperfx\Framework\Constants\ErrorCodeX::getGrpcStatus($strCode);
            }
        } else {
            $httpCode = (StatusCode::HTTP_CODE_MAPPING[$code] ?? 500);
            $grpcCode = isset(StatusCode::HTTP_CODE_MAPPING[$code]) ? $code : StatusCode::UNKNOWN;
        }
        return [$httpCode, $grpcCode];
    }

    /**
     * Transfer the non-standard response content to a standard response object.
     */
    protected function transferToResponse(int $httpCode, int $grpcCode, string $message, ResponseInterface $response): ResponseInterface
    {
        $response = $response->withAddedHeader('Content-Type', 'application/grpc')
            ->withAddedHeader('trailer', 'grpc-status, grpc-message')
            ->withStatus($httpCode);

        if (method_exists($response, 'withTrailer')) {
            $response = $response->withTrailer('grpc-status', (string) $grpcCode)->withTrailer('grpc-message', (string) $message);
        }
        Context::set(ContextResponseConst::CTX_RESPONSE_STATUS_CODE, $httpCode);
        Context::set(ContextResponseConst::CTX_RESPONSE_GRPC_STATUS, $grpcCode);
        Context::set(ContextResponseConst::CTX_RESPONSE_BODY_SIZE, $response->getBody()->getSize());
        return $response;
    }
}