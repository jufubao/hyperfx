<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\Framework\Exception\Handler;

use Hyperf\Context\Context;
use Hyperf\GrpcClient\Exception\GrpcClientException;
use Hyperf\Utils\Codec\Json;
use Hyperfx\Framework\Constants\ContextResponseConst;
use Hyperfx\Framework\Constants\ErrorCode;
use Hyperfx\Framework\Constants\ErrorCodeX;
use Hyperfx\Framework\Exception\AppException;
use Hyperfx\Framework\Support\Response;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\ExceptionHandler\Formatter\FormatterInterface;
use Hyperf\Grpc\StatusCode;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class ApiExceptionHandler extends ExceptionHandler
{
    use BaseExceptionHandler;

    const DEFAULT_HTTP_CODE = 500;

    /**
     * @var string
     */
    private $appEnv = 'prod';

    public function __construct(protected StdoutLoggerInterface $logger, protected FormatterInterface $formatter, protected ConfigInterface $config) {
        $this->appEnv = $this->config->get("app_env", "prod");
    }

    public function handle(Throwable $throwable, ResponseInterface $response)
    {
        $code = $this->geCode($throwable);

        $logLevel = $this->getLogLevel($throwable, $code);

        $this->logger->log($logLevel, $this->formatter->format($throwable));

        return $this->transferToResponse($code, $throwable, $response);
    }

    /**
     * 处理错误码
     * 如果大小600，则三前位作为httpCode
     * @return array $httpCode, $bodyCode
     */
    private static function handleCode(int $code): int {
        if ($code  < 1) {
            return self::DEFAULT_HTTP_CODE;
        }

        // Grpc error code
        if ($code < 200 && $code > 0) {
            return StatusCode::HTTP_CODE_MAPPING[$code] ?? self::DEFAULT_HTTP_CODE;
        }

        // Http Code
        if ($code < 600) {
            return $code;
        }

        $httpCode = (int) substr((string) $code, 0, 3);
        return $httpCode;
    }

    public function isValid(Throwable $throwable): bool
    {
        return true;
    }

    /**
     * Transfer the non-standard response content to a standard response object.
     */
    protected function transferToResponse(int $code, Throwable $throwable, ResponseInterface $response): ResponseInterface
    {
        $httpCode = self::handleCode($code);

        $response = $response->withAddedHeader('Content-Type', 'application/json')->withHeader('Server', 'Jufubao');

        if ($this->isTimeoutFromThrowable($throwable)) {
            $message = '请求超时，请稍后重试';
        } else {
            $message = !empty($throwable->getMessage()) ? $throwable->getMessage() : 'Internal Server Error.';
        }

        $extend = [];
        $data = [];

        $isDebug = true;
        if ($throwable instanceof AppException) {
            $data = $throwable->getContext();
            $isDebug = $throwable->isDebug();
        }

        // 开发模式注入debug信息
        $isDebug && $this->withDebug($extend, $throwable);

        $bodyCode = $this->handleBodyStringCode($httpCode, $message);
        $json = Response::json($bodyCode, $message, $data, $extend);

        // 阻止异常冒泡
        $this->stopPropagation();
        Context::set(ContextResponseConst::CTX_RESPONSE_STATUS_CODE, $httpCode);
        $response = $response->withStatus($httpCode)->withBody(new SwooleStream(Json::encode($json)));
        Context::set(ContextResponseConst::CTX_RESPONSE_BODY_SIZE, $response->getBody()->getSize());
        return $response;
    }

    private function handleBodyStringCode(int $httpCode, string &$message) {
        if (str_contains($message, '##')) {
            $arr = explode('##', $message);
            $message = $arr[1] ?? $message;
            return $arr[0] ?? 'Unknown';
        }
        return ErrorCode::$MAPPING[$httpCode] ?? 'Unknown';
    }

    /**
     * 输出debug信息.
     */
    protected function withDebug(array &$extend, Throwable $e): void
    {
        // 仅prod不开启
        if (0 == strcmp($this->appEnv, 'prod')) {
            return;
        }
        $extend['debug'] = [
            'code' => $e->getCode(),
            'message' => $e->getMessage(),
            'file' => $e->getFile() . ':' . $e->getLine(),
            'trace' => explode("\n", $e->getTraceAsString()),
            'previous' => $e->getPrevious(),
        ];
    }
}