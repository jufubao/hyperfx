<?php

declare(strict_types=1);

namespace Hyperfx\Framework\Server;

use Hyperf\Context\Context;
use Hyperf\HttpMessage\Server\Response as Psr7Response;
use Hyperf\Utils\Network;
use Hyperfx\Framework\Constants\ContextResponseConst;
use Hyperfx\Framework\Constants\ErrorCode;
use Hyperfx\Framework\Constants\ErrorCodeX;
use Hyperfx\Framework\Logger\Logx;
use Hyperfx\Utils\NetworkUtil;
use stdClass;

class GrpcServer extends \Hyperf\GrpcServer\Server
{
    use ServerTrait;
    
    public function onRequest($request, $response): void {
        $path = $request->server['request_uri'] ?? '';
        if ($path === '/ping') {
            $psr7Response = (new Psr7Response())->withStatus(200)->withContent('ok');
            $this->responseEmitter->emit($psr7Response, $response);
            return;
        }

        Logx::nextTraceId($request->header['x-b3-traceid'] ?? '');
        
        parent::onRequest($request, $response);

        // 写入日志
        $this->writeLog($request);
    }

    /**
     * @param \Swoole\Http\Request $request
     */
    public function writeLog($request) {
        $startTime = $request->server['request_time_float'];
        $endTime = microtime(true);
        $diffTime = $endTime - $startTime;
        $duration = (int) ($diffTime == 0 ? 0 : round($diffTime * 1000));

        $path = isset($request->server['query_string']) ? sprintf('%s?%s', $request->server['request_uri'] ?? '', $request->server['query_string'] ?? '') : ($request->server['request_uri'] ?? '');
        $contentLength = Context::get(ContextResponseConst::CTX_REQUEST_BODY_SIZE, 0);

        $bytesSent = Context::get(ContextResponseConst::CTX_RESPONSE_BODY_SIZE, 0);
        $statusCode = Context::get(ContextResponseConst::CTX_RESPONSE_STATUS_CODE, 200);
        $gRpcStatus = Context::get(ContextResponseConst::CTX_RESPONSE_GRPC_STATUS, 0);
        $body = Context::get(ContextResponseConst::CTX_RESPONSE_BODY, '');

        Context::destroy(ContextResponseConst::CTX_REQUEST_BODY_SIZE);
        Context::destroy(ContextResponseConst::CTX_RESPONSE_BODY_SIZE);
        Context::destroy(ContextResponseConst::CTX_RESPONSE_STATUS_CODE);
        Context::destroy(ContextResponseConst::CTX_RESPONSE_GRPC_STATUS);
        Context::destroy(ContextResponseConst::CTX_RESPONSE_BODY);

        $message = [
            'authority' => $request->header['host'] ?? '',
            'bytes_received' => (int) $contentLength,
            'bytes_sent' => $bytesSent,
            'duration' => $duration,
            'remote_address' => sprintf('%s:%s', NetworkUtil::getClientIpFromSwoole($request), $request->server['remote_port'] ?? 0),
            'method' => $request->server['request_method'] ?? '',
            'path' => $path,
            'protocol' => $request->server['server_protocol'] ?? '',
            'user-agent' => $request->header['user-agent'] ?? '-',
            'body' => $body,
            'response_code' => $statusCode,
            'grpc-status' => $gRpcStatus,
            'request_time' => self::getTime($startTime),
            'master_time' => date(DATE_ATOM, $request->server['master_time'] ?? 0),
            'stream_id' => $request->streamId,
            'upstream_host' => sprintf('%s:%s', Network::ip(), $request->server['server_port'] ?? 0),
        ];
        
        Logx::get()->info('Request', $message);
    }
}
