<?php

declare(strict_types=1);

namespace Hyperfx\Framework\Server;

trait ServerTrait
{
    protected static function getTime($startTime): string {
        $arr = explode('.', (string) $startTime);
        $date = date('Y-m-d\TH:i:s', (int) $arr[0] ?? 0);
        return sprintf('%s.%sZ', $date, $arr[1] ?? 0);
    }
}