<?php

declare(strict_types=1);

namespace Hyperfx\Framework\Server;

use Hyperf\Context\Context;
use Hyperf\Coordinator\Constants;
use Hyperf\Coordinator\CoordinatorManager;
use Hyperf\HttpMessage\Server\Response as Psr7Response;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\MiddlewareManager;
use Hyperf\HttpServer\Router\Dispatched;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Network;
use Hyperf\Utils\SafeCaller;
use Hyperfx\Framework\Constants\ContextResponseConst;
use Hyperfx\Framework\Constants\ErrorCode;
use Hyperfx\Framework\Constants\ErrorCodeX;
use Hyperfx\Framework\Logger\Logx;
use Hyperfx\Utils\NetworkUtil;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\ResponseInterface;
use stdClass;
use Swoole\Http\Response;

class HttpServer extends \Hyperf\HttpServer\Server
{
    use ServerTrait;
    /**
     * @param \Swoole\Http\Request $request
     * @param \Swoole\Http\Response $response
     */
    public function onRequest($request, $response): void {
        $path = $request->server['request_uri'] ?? '';
        if ($path === '/ping') {
            $psr7Response = (new Psr7Response())->withStatus(200)->withContent('ok');
            $this->responseEmitter->emit($psr7Response, $response);
            return;
        }
        
        Logx::nextTraceId($request->header['x-b3-traceid'] ?? '');
        
        parent::onRequest($request, $response);

        // 写入日志
        $this->writeLog($request);
    }
    
    /**
     * @param \Swoole\Http\Request $request
     */
    public function writeLog($request) {
        $startTime = $request->server['request_time_float'];
        $endTime = microtime(true);
        $diffTime = $endTime - $startTime;
        $duration = (int) ($diffTime == 0 ? 0 : round($diffTime * 1000));
        $path = isset($request->server['query_string']) ? sprintf('%s?%s', $request->server['request_uri'] ?? '', $request->server['query_string'] ?? '') : ($request->server['request_uri'] ?? '');
        $contentLength = $request->header['content-length'] ?? 0;


        $bytesSent = Context::get(ContextResponseConst::CTX_RESPONSE_BODY_SIZE, 0);
        $statusCode = Context::get(ContextResponseConst::CTX_RESPONSE_STATUS_CODE, 200);
        $body = Context::get(ContextResponseConst::CTX_RESPONSE_BODY, '');

        Context::destroy(ContextResponseConst::CTX_REQUEST_BODY_SIZE);
        Context::destroy(ContextResponseConst::CTX_RESPONSE_BODY_SIZE);
        Context::destroy(ContextResponseConst::CTX_RESPONSE_STATUS_CODE);
        Context::destroy(ContextResponseConst::CTX_RESPONSE_BODY);

        $message = [
            'authority' => $request->header['host'] ?? '',
            'bytes_received' => (int) $contentLength,
            'bytes_sent' => $bytesSent,
            'duration' => $duration,
            'remote_address' => sprintf('%s:%s', NetworkUtil::getClientIpFromSwoole($request), $request->server['remote_port'] ?? 0),
            'method' => $request->server['request_method'],
            'path' => $path,
            'protocol' => $request->server['server_protocol'] ?? '',
            'header' => [
                'content-type' => $request->header['content-type'] ?? '',
                'user-agent' => $request->header['user-agent'] ?? '-',
                'referer' => $request->server['referer'] ?? '-',
            ],
            'body' => $body,
            'response_code' => $statusCode,
            'request_time' => self::getTime($startTime),
            'master_time' => date(DATE_ATOM, $request->server['master_time'] ?? 0),
            'stream_id' => $request->streamId,
            'upstream_host' => sprintf('%s:%s', Network::ip(), $request->server['server_port'] ?? 0),
        ];

        if (isset($request->header['x-site'])) {
            $message['header']['x-site'] = $request->header['x-site'];
        }

        if (isset($request->header['x-jfb-user-id'])) {
            $message['header']['x-jfb-user-id'] = $request->header['x-jfb-user-id'];
        }

        if (isset($request->header['x-jfb-project-id'])) {
            $message['header']['x-jfb-project-id'] = $request->header['x-jfb-project-id'];
        }

        if (isset($request->header['x-version'])) {
            $message['header']['x-version'] = $request->header['x-version'];
        }

        if (isset($request->header['x-jfb-os'])) {
            $message['header']['x-jfb-os'] = $request->header['x-jfb-os'];
        }

        if (Context::has('admin-userinfo')) {
            $userInfo = Context::get('admin-userinfo');
            $message['header']['admin-user'] = [
                'user_id' => $userInfo['user_id'],
                'partner_id' => $userInfo['partner_id'],
                'current_partner_id' => $userInfo['current_partner_id'],
            ];
        }

        Logx::get()->info('Request', $message);
    }
}
