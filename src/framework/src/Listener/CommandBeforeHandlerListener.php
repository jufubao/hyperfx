<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\Framework\Listener;

use Hyperf\Command\Event\BeforeHandle;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperfx\Framework\Logger\Logx;

class CommandBeforeHandlerListener implements ListenerInterface
{
    public function listen(): array
    {
        return [
            BeforeHandle::class,
        ];
    }

    public function process(object $event): void
    {
        Logx::nextTraceId();
    }
}
