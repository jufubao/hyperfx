<?php
declare(strict_types=1);

namespace Hyperfx\Utils;

class RunModeUtil {

    /**
     * http 模式
     */
    public static function isHttp(): bool {
        // 因为config加载太靠后，所以这里必须用env才能兼容
        return env('RUN_MODE', 'http') === 'http';
    }

    /**
     * 守护进程
     */
    public static function isDaemonProcess(): bool {
        return env('RUN_MODE', 'http') === 'DaemonProcess';
    }

    /**
     * 数据同步
     */
    public static function isDataSyncProcess(): bool {
        return env('RUN_MODE', 'http') === 'DataSyncProcess';
    }

    /**
     * 自定义模式
     */
    public static function is(string $mode): bool {
        return env('RUN_MODE', 'http')=== $mode;
    }
}