<?php
declare(strict_types=1);

namespace Hyperfx\Utils;

class PageUtil
{
    private int $pageSize;
    private string $pageToken;
    private int $totalSize = -1; // 总大小,可选
    private int $curPageSize = -1; // 当前页大小, 可选
    public function __construct($request)
    {
        $this->pageToken  = $request->getPageToken();
        $this->pageSize = $request->getPageSize();
    }

    /**
     * 设置总条数
     */
    public function setTotalSize(int $totalSize): void
    {
        $this->totalSize = $totalSize;
    }
    
    /**
     * 设置当前页大小
     */
    public function setCurPageSize(int $curPageSize): void
    {
        $this->curPageSize = $curPageSize;
    }
    
    
    /**
     * 当前页
     */
    public function getPage(): int {
        return (int) $this->pageToken ?: 1;
    }

    public function getOffset(): int
    {
        $skip = (int) $this->pageToken;
        if ($skip < 2 || $this->pageSize < 1){
            return 0;
        }
        return ($skip - 1) * $this->pageSize;
    }

    /**
     * 每页数量
     */
    public function getPerPage(): int {
        return (int) $this->pageSize ?: 20;
    }

    /**
     * 获取 next_page_token
     */
    public function getNextPageToken(): string {
        if ($this->totalSize > -1) {
            // 如果每页大小*当前页小于或等于时,则没有页面了
            if ($this->totalSize <= $this->getPage() * $this->getPerPage()) {
                return '';
            }
            return $this->_getNextPageToken();
        }

        if ($this->curPageSize > -1) {
            // 当果当前页小于实际每页大小时,则没有下一页了
            if ($this->curPageSize < $this->getPerPage()) {
                return '';
            }
        }
        return $this->_getNextPageToken();
    }

    private function _getNextPageToken(): string {
        return (string) ($this->getPage() + 1);
    }
}