<?php
declare(strict_types=1);

namespace Hyperfx\Utils;

use Hyperf\Contract\ConfigInterface;

class AppEnvUtil {

    /**
     * 生产
     */
    public static function isProd(): bool {
        // 因为config加载太靠后，所以这里必须用env才能兼容
        return env('APP_ENV', '') === 'prod';
    }

    /**
     * 开发
     */
    public static function isDev(): bool {
        return env('APP_ENV', '') === 'dev';
    }

    /**
     * 本地
     */
    public static function isLocal(): bool {
        return env('APP_ENV', '') === 'local';
    }
}