<?php
namespace Hyperfx\Utils;

use Hyperf\Grpc\StatusCode;
use \Throwable;
use \Closure;
use Hyperfx\Framework\Logger\Logx;
use Hyperf\Utils\Exception\ParallelExecutionException;
use Swoole\Coroutine\Channel;
use Hyperf\Utils\Parallel as HyperfParallel;
use \Google\Protobuf\Internal\Message;
use Hyperf\Utils\Codec\Json;
use Hyperf\GrpcClient\Exception\GrpcClientException;

/**
 * 并行RpcUtil
 *
 * 方法Create       创建util实例
 * 方法Add          添加RPC闭包，获得闭包Tag
 * 方法Exec         将所有添加的RPC闭包并行执行
 * 方法Success      判断RPC是否执行成功
 * 方法GetResponse  通过闭包Tag获取RPC执行结果
 * 方法Relay        添加接力
 *
 * 调用举例：
 * $userRpcFunc = function () use($userId)
 * {
 *      $request = new UserDemoRequest();
 *      $request->setUserId($userId);
 *      return UserDemoClient::create()->GetUser($request);
 * };
 * $productRpcFunc = function() use($productId)
 * {
 *      $request = new ProductDemoRequest();
 *      $request->setProductId($productId);
 *      return ProductDemoClient::create()->GetProduct($request);
 * };
 *
 * $pr = ParallelRpc::create();
 * $prUserTag = $pr->add($userRpcFunc);
 * $prProductTag = $pr->add($productRpcFunc);
 * $pr->exec();
 *
 * if (! $pr->success($prUserTag))
 * {
 *      $pr->grpcClientException($prUserTag);
 * }
 * if (! $pr->success($prProductTag))
 * {
 *      $pr->grpcClientException($prProductTag);
 * }
 * $userResponse = $pr->getResponse($prUserTag);
 * $productResponse = $pr->getResponse($prProductTag);
 *
 * 
 * 调用举例（接力功能）:
 * $userRpcFunc = function () use($userId)
 * {
 *      $request = new UserDemoRequest();
 *      $request->setUserId($userId);
 *      return UserDemoClient::create()->GetUser($request);
 * };
 * $productRpcFunc = function() use($productId)
 * {
 *      $request = new ProductDemoRequest();
 *      $request->setProductId($productId);
 *      return ProductDemoClient::create()->GetProduct($request);
 * };
 * $orderRpcFunc = function() use($orderId)
 * {
 *      $request = new OrderDemoRequest();
 *      $request->setOrderId($productId);
 *      return OrderDemoClient::create()->GetOrder($request);
 * };
 *
 * $pr = ParallelRpc::create();
 * $prUserTag = $pr->add($userRpcFunc);
 * 
 * // product与order按顺序串行执行，同时整体上与user属于并行执行关系
 * [$prProductTag, $prOrderTag] = $pr->relay(
 *      $pr->relayItem($productFunc),
 *      $pr->relayItem($orderFunc),
 * );
 * $pr->exec();
 *
 * if (! $pr->success($prUserTag))
 * {
 *      $pr->grpcClientException($prUserTag);
 * }
 * if (! $pr->success($prProductTag))
 * {
 *      $pr->grpcClientException($prProductTag);
 * }
 * if (! $pr->success($prOrderTag))
 * {
 *      $pr->grpcClientException($prOrderTag);
 * }
 * $userResponse = $pr->getResponse($prUserTag);
 * $productResponse = $pr->getResponse($prProductTag);
 * $orderResponse = $pr->getResponse($prOrderTag);
 * 
 */

class ParallelRpcUtil
{
    // Hyperf Parallel 实例
    private HyperfParallel|null $hp = null;
    // Swoole Channel 实例
    private Channel|null $chan = null;
    // Swoole Channel 长度
    private int $chanLen = 0;
    // 临时RPC闭包存放区
    private array $tempFuncs = [];
    // 临时RPC接力存放区
    private array $tempRelays = [];
    // RPC请求起始时间
    private array $beginTimes = [];
    // RPC请求结束时间
    private array $finishTimes = [];
    // RPC请求Response Map
    private array $execResponseMap = [];
    // RPC请求Status Map
    private array $execStatusMap = [];

    private function __construct()
    {
    }

    // 创建实例
    public static function create(): self
    {
        $instance = new self();
        $instance->init();
        $instance->cleanExeResult();
        return $instance;
    }

    // 初始化
    private function init(): void
    {
        $this->hp = new HyperfParallel();
        $this->chan = null;
        $this->tempFuncs = [];
        $this->beginTimes = $this->finishTimes = [];
    }

    // 添加一个RPC闭包到临时存放区
    public function add(Closure $func, string $tagName = '', ...$args): string
    {
        // 当未指定Tag则生成一个
        $tag = $tagName ?: self::generateTag();
        $this->tempFuncs[] = [$tag, $func, $args];
        $this->chanLen++;
        self::logInfo('ParallelRpc', 'AddFunc', 'TagName', $tag);
        return $tag;
    }

    public function addMultiFuncs(array $funcs): array
    {
        $tags = [];
        foreach ($funcs as $func)
        {
            $tags[] = $this->add($func);
        }
        return $tags;
    }

    public function addMultiFuncsWithArgs(array $funcsWithArgs): array
    {
        $tags = [];
        foreach ($funcsWithArgs as [$func, $args])
        {
            $tags[] = $this->add($func, ...$args);
        }
        return $tags;
    }
    
    public function relay(array $relay): array
    {
        $tags = [];
        [$relay, $tags] = self::checkRelay($relay);    
        $this->tempRelays[] = $relay;
        $this->chanLen += count($relay);
        self::logInfo('ParallelRpc', 'AddRelay', 'TagNames', $tags);
        return $tags;
    }
    
    // 执行请求
    public function exec(): void
    {
        try
        {
            self::logInfo('ParallelRpc', 'Exec');
            $this->prepare();
            $this->hp->wait();
            $chanLen = $this->chan->length();
            self::logInfo('ParallelRpc', 'chanLen', $chanLen);
            // 将Channel中的RPC结果数据保存
            for ($i = 0; $i < $chanLen; $i++)
            {
                [$tag, [$response, $status]] = $this->chan->pop();
                self::logInfo('ParallelRpc', 'TagName', $tag, 'TimeUsed', ($this->finishTimes[$tag] - $this->beginTimes[$tag]));
                $this->execResponseMap[$tag] = $response;
                $this->execStatusMap[$tag] = $status;
            }
        }
        catch (ParallelExecutionException $e)
        {
            $messages = [];
            foreach ($e->getThrowables() as $throwable)
            {
                $messages[] = 'File:'. $throwable->getFile(). ' Line:'. $throwable->getLine(). ' Message:'. $throwable->getMessage();
            }
            self::logError('ParallelRpc', 'ParallelExecutionException', 'Tag', $messages);
        }
        catch (Throwable $e)
        {
            self::logError('ParallelRpc', 'Throwable', $e->getMessage());
        }
        finally
        {
            $this->init();
        }
    }

    // 清除请求结果数据
    public function cleanExeResult(): void
    {
        $this->execResponseMap = $this->execStatusMap = [];
    }

    // 获取请求结果
    public function getExeResult(string $tag): array
    {
         return [$this->execResponseMap[$tag] ?? null, $this->execStatusMap[$tag] ?? null];
    }

    // 获取RPC请求Response
    public function getResponse(string $tag): Message|string
    {
        return $this->execResponseMap[$tag] ?? '';
    }

    // 获取RPC请求Status
    public function getStatus(string $tag): int
    {
        return $this->execStatusMap[$tag] ?? 0;
    }

    // 抛出GrpcClientException异常
    public function grpcClientException(string $tag): void
    {
        $response = is_string($this->getResponse($tag)) ? $this->getResponse($tag) : '';
        throw new GrpcClientException((string) $response, (int) $this->getStatus($tag));
    }

    // 判断RPC请求是否成功
    public function success(string $tag): bool
    {
        [$response, $status] = $this->getExeResult($tag);
        if (is_string($response))
        {
            self::logError('ParallelRpc', 'ResponseError', 'Tag', $tag, $response);
        }
        return StatusCode::OK === $status && is_object($response);
    }

    // 判断RPC请求是否失败
    public function failed(string $tag): bool
    {
        return ! $this->success($tag);
    }

    // 准备工作
    private function prepare(): void
    {
        self::logInfo('ParallelRpc', 'chanLen', $this->chanLen);
        if (! $this->chanLen)
        {
            self::logError('ParallelRpc', 'Prepare', 'EmptyChan');
            throw new \Exception('未添加需要处理的RPC');
        }
        $this->chan = new Channel($this->chanLen);
        $this->prepareFuncs();
        $this->prepareRelays();
    }

    // 将临时存放区中的闭包放到Hyperf Parallel中，并清空
    private function prepareFuncs(): void
    {
        foreach ($this->tempFuncs as [$tag, $func, $args])
        {
            $this->hp->add(function() use($tag, $func, $args)
            {
                self::logInfo('ParallelRpc', 'ExecuteStart', $tag);
                // 计时开始
                $this->beginTimes[$tag] = microtime(true);
                // RPC请求的Tag和请求结果存入Channel
                $this->chan->push([$tag, $func(...$args)]);
                // 计时结束
                $this->finishTimes[$tag] = microtime(true);
                self::logInfo('ParallelRpc', 'ExecuteFinish', $tag);
            });
        }
        $this->tempFuncs = [];
    }

    // 将临时存放区中的接力放到Hyperf Parallel中，并清空
    private function prepareRelays(): void
    {
        foreach ($this->tempRelays as $relay)
        {
            $this->hp->add(function() use($relay)
            {
                foreach ($relay as [$func, $tag, $args])
                {
                    self::logInfo('ParallelRpcRelay', 'ExecuteStart', $tag);
                    // 计时开始
                    $this->beginTimes[$tag] = microtime(true);
                    // RPC请求的Tag和请求结果存入Channel
                    $this->chan->push([$tag, $func(...$args)]);
                    // 计时结束
                    $this->finishTimes[$tag] = microtime(true);
                    self::logInfo('ParallelRpcRelay', 'ExecuteFinish', $tag);
                }
            });
        }
        $this->tempRelays = [];
    }
    
    // 生成一个随机的Tag
    private static function generateTag(): string
    {
        return substr(md5(microtime(true). rand(0, 9999)), 0, 8);
    }

    private static function logInfo(...$content): void
    {
        Logx::get()->info(Json::encode([...$content]));
    }

    private static function logError(...$content): void
    {
        Logx::get()->error(Json::encode([...$content]));
    }
    
    private static function checkRelay(array $relay)
    {
        $tags = [];
        $funcList = [];
        foreach ($relay as [$func, $tagName, $args])
        {
            if (! $tagName)
            {
                $tagName = $tagName ?: self::generateTag();
            }
            $tags[] = $tagName;
            $funcList[] = [$func, $tagName, $args];
        }
        return [$funcList, $tags];
    }
    
    public function relayItem(Closure $func, string $tagName = '', ...$args): array
    {
        return [$func, $tagName, $args];
    }
}

