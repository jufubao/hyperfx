<?php
declare(strict_types=1);

namespace Hyperfx\Utils;

class ArrayUtil
{
    // 排序成树结构
    public static function sortTree(&$arr, $subName = 'children', $idName = 'id', $parentIdName = 'parent_id') {
        $temp = array();
        foreach ($arr as $row) {
            $temp[$row[$idName]] = $row;
        }
        foreach ($temp as $row) {
            $temp[$row[$parentIdName]][$subName][$row[$idName]] = &$temp[$row[$idName]];
        }
        return isset($temp[0][$subName]) ? $temp[0][$subName] : array();
    }
}