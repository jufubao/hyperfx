<?php
declare(strict_types=1);

namespace Hyperfx\Utils;
use Hyperf\Redis\RedisFactory;
use Hyperf\Redis\RedisProxy;
use Hyperf\Snowflake\IdGeneratorInterface;
use Hyperf\Snowflake\Meta;
use Hyperf\Utils\ApplicationContext;

class SnowflakeX
{
    /**
     * 生成雪花算法唯一ID
     *
     * @param int $id 用户ID或供应商ID，必需是自增或随机产生的数值，不可用雪花算法生成
     * @param int $num 分表总数量 2的倍数
     *
     * @return array [id, tableId] id=生成的唯一ID, tableId 分表总数
     */
    public static function generate(int $id, int $tableNum = 2): array {
        $container = ApplicationContext::getContainer();
        $generator = $container->get(IdGeneratorInterface::class);
        /* @var $meta Meta*/
        $meta = $generator->getMetaGenerator()->generate();
        $val = $id % 1024;
        $centerId = (int) floor($val / 32);
        $workerId = $val % 32;
        $meta->setDataCenterId($centerId);
        $meta->setWorkerId($workerId);
        
        $conf = config('snowflakex');
        if (!empty($conf) && !empty($conf['pool']) && !empty($conf['key'])) {
            $tableId = $centerId * 32 + $workerId;
            $redis = ApplicationContext::getContainer()->get(RedisFactory::class)->get($conf['pool']);
            $key = sprintf($conf['key'], $tableId);
            $id = (int) $redis->incr($key);
            if ($id > 100000000) {
                // 避免越界
                $redis->delete($key);
            }
            $meta->setSequence($id % 4096);
        }
        
        $newId = $generator->generate($meta);
        if ($newId < 1) {
            throw new \Exception('Internal errors', 500);
        }
        return [$newId, $val % $tableNum];
    }

    /**
     * 只生成tableid
     *
     * @param int $id 用户ID或供应商ID，必需是自增或随机产生的数值，不可用雪花算法生成
     * @param int $num 分表总数量 2的倍数
     *
     * @return int tableId 分表总数
     */
    public static function generateTableId(int $id, int $tableNum = 2): int {
        return $id % 1024 % $tableNum;
    }
    
    /**
     * 解析出分表的ID
     *
     * @param int $id 雪花算法生成的ID
     * @param int $tableNum 分表总数 2的倍数
     */
    public static function parseTableId(int|string $id, int $tableNum = 2): int {
        if (is_string($id)) {
            // 如果有订单号前缀则删除
            if (!is_numeric(substr($id, 0, 1))) {
                $id = substr($id, 1);
            }
            $id = (int) $id;
        }
        $container = ApplicationContext::getContainer();
        $generator = $container->get(IdGeneratorInterface::class);
        $deOrderId = $generator->degenerate($id);
        $tableId = $deOrderId->getDataCenterId() * 32 + $deOrderId->getWorkerId();
        return $tableId % $tableNum;
    }
}
