<?php
declare(strict_types=1);

namespace Hyperfx\Utils;

class PageFunc
{
    public static function getOffset(int $skip = 1, int $size = 20): int
    {
        if ($skip < 1 || $size < 1){
            return 0;
        }
        return ($skip - 1) * $size;
    }
}