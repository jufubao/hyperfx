<?php
declare(strict_types=1);

namespace Hyperfx\Utils;

use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Network;

class RequestUtil {
    /**
     * 获取真实的host
     */
    public static function getHost(string|null $host = null): string {
        if (is_null($host) || empty($host)) {
            $request = ApplicationContext::getContainer()->get(RequestInterface::class);
            $host = $request->getUri()->getHost();
        }
        if (str_ends_with($host, '.apis-proxy.jufubao.cn')) {
            $totalLen = strlen($host);
            // $hostLen = strlen('.apis-proxy.jufubao.cn');
            $hostLen = 22;
            $endIndex = $totalLen - $hostLen;
            $virHost = substr($host, 0, $endIndex);
            return str_replace('__', '.', $virHost);
        }
        return $host;
    }
}