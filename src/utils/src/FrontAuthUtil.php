<?php

declare(strict_types=1);

namespace Hyperfx\Utils;

use Hyperf\Context\Context;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperfx\Framework\Constants\ErrorCodeX;
use Hyperfx\Framework\Exception\AppException;
use Hyperfx\Framework\Logger\Logx;

class FrontAuthUtil
{
    public static function getPartnerId(): int {
        return Context::getOrSet('jfb-partner-id', function () {
            // partner
            $partnerId = ApplicationContext::getContainer()->get(RequestInterface::class)->getHeaderLine('x-jfb-partner-id');
            if (empty($partnerId)) {
                Logx::get()->error('x-site[partner_id] 不能为空');
                throw new AppException(ErrorCodeX::INVALID_ARGUMENT, 'x-site 不能为空');
            }
            return (int) $partnerId;
        });
    }

    public static function getSiteId(): string {
        return Context::getOrSet('jfb-site-id', function () {
            // site_id
            $siteId = ApplicationContext::getContainer()->get(RequestInterface::class)->getHeaderLine('x-jfb-site-id', '');
            if (empty($siteId)) {
                Logx::get()->error('x-site[site_id] 不能为空');
                throw new AppException(ErrorCodeX::INVALID_ARGUMENT, 'x-site 不能为空');
            }
            return $siteId;
        });
    }

    public static function getNamespace(): string {
        return Context::getOrSet('jfb-namespace', function () {
            // namespace
            $namespace = ApplicationContext::getContainer()->get(RequestInterface::class)->route('xnamespace', '');
            if (empty($namespace)) {
                Logx::get()->error('namespace 不能为空');
                throw new AppException(ErrorCodeX::INVALID_ARGUMENT, 'namespace 不能为空');
            }
            return $namespace;
        });
    }

    public static function getPlatformId(): int {
        return (int) ApplicationContext::getContainer()->get(RequestInterface::class)->getHeaderLine('x-jfb-platform-id');
    }

    public static function getUserId(bool $required = true): int {
        $userId = (int) ApplicationContext::getContainer()->get(RequestInterface::class)->getHeaderLine('x-jfb-user-id');
        if ($required && $userId < 1) {
            throw new AppException(ErrorCodeX::UNAUTHENTICATED);
        }
        return $userId;
    }

    public static function getUserinfo(bool $required = true): array {
        return Context::getOrSet('jfb-user', function () use ($required) {
            $userInfoString = ApplicationContext::getContainer()->get(RequestInterface::class)->getHeaderLine('x-jfb-user');
            if ($required && empty($userInfoString)) {
                throw new AppException(ErrorCodeX::UNAUTHENTICATED);
            }
            if (empty($userInfoString)) {
                return [];
            }
            $userInfoArray = explode(';', $userInfoString);
            $data = [];
            foreach ($userInfoArray as $line) {
                $arr = explode('=', $line);
                $data[$arr[0]] = $arr[1];
            }
            $data['user_id'] = self::getUserId($required);
            return $data;
        });
    }

    /**
     * 获取当前登录的卡信息
     */
    public static function getCard(): array {
        return Context::getOrSet('jfb-card', function () {
            $cardInfoString = ApplicationContext::getContainer()->get(RequestInterface::class)->getHeaderLine('x-jfb-card');
            if (empty($cardInfoString)) {
                return [];
            }
            $cardInfoArray = explode(';', $cardInfoString);
            $data = [];
            foreach ($cardInfoArray as $line) {
                $arr = explode('=', $line);
                $data[$arr[0]] = $arr[1];
            }
            return $data;
        });
    }

    /**
     * 获取当前登录的卡号
     */
    public static function getCardNumber(): string {
        $card = self::getCard();
        return $card['card_number'] ?? '';
    }
}
