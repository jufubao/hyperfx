<?php
namespace Hyperfx\Utils;

use Hyperf\Database\Model\Builder;
use Hyperf\Utils\Str;

class ModelUtil {
    /**
     * 获取最后一次执行SQL
     *
     * 此方法要在执行语句之前调取，比如 fist,find,get之前
     * 此方式不会引起内存泄漏，缺点必须是model形式运行的才能捕获
     */
    public static function getQuerySql(Builder $model): string
    {
        $sql = $model->toSql();
        $bindings = $model->getBindings();
        if (!empty($bindings)) {
            foreach ($bindings as $value) {
                $sql = Str::replaceFirst('?', "'{$value}'", $sql);
            }
        }
       return $sql;
    }
}