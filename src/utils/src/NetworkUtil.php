<?php
declare(strict_types=1);

namespace Hyperfx\Utils;

use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Network;

class NetworkUtil {

    public static function ip(): string {
        return env("X-HOSTNAME", Network::ip());
    }

    /**
     * 获取客户端Ip
     */
    public static function getClientIp(): string {
        $request = ApplicationContext::getContainer()->get(RequestInterface::class);
        $ip = $request->header('x-real-ip');
        if (!empty($ip)) {
            return $ip;
        }

        $ip = $request->header('x-forwarded-for');
        if (!empty($ip)) {
            return $ip;
        }

        $serverParams = $request->getServerParams();
        return $serverParams['remote_addr'] ?? '';
    }

    /**
     * 获取客户端Ip
     *
     * @param \Swoole\Http\Request $request
     */
    public static function getClientIpFromSwoole($request): string {
        $ip = $request->header['x-real-ip'] ?? '';
        if (!empty($ip)) {
            return $ip;
        }

        $ip = $request->header['x-forwarded-for'] ?? '';
        if (!empty($ip)) {
            return $ip;
        }

        return $request->server['remote_addr'];
    }
}