<?php

namespace Hyperfx\Utils;

use Hyperf\Database\Model;
use \Google\Protobuf\Internal\RepeatedField;
use \Google\Protobuf\Internal\Message;
use Google\Protobuf\Internal\MapField;

class ProtobufUtil
{

    public  static function repeatedForeach(\Google\Protobuf\Internal\RepeatedField $data, callable $handle, bool $checkIsEmpty = true): array {
        $result = [];
        if ($data->count() == 0) {
            return $result;
        }
        foreach ($data as $item) {
            $val = $handle($item);
            if ($checkIsEmpty) {
                if (!empty($val)) {
                    $result[] = $val;
                }
            } else {
                $result[] = $val;
            }
        }
        return $result;
    }

    /**
     * @param RepeatedField|array   $data   需要处理的索引数组或者RepeatedField结构的数据
     * @param array|null            $adjust 字段值调整，一维关联数组，键为字段名，值为调整方法或调整值。如 ['create_time' => function($value){return date('Y-m-d', $value);}]
     * @param array|null            $remove 移除的字段，一维索引数组，值为要移除的字段名。如 ['create_time', 'update_time']
     * @param array|null            $rename 改名的字段，一维关联数组，键为字段名，值为新字段名。如 ['create_time' => 'created_datetime']
     * @return array
     */
    public static function repeatedToAutoGenArray(RepeatedField|array $data, ?array $adjust = [], ?array $remove =[], ?array $rename = []): array {
        $inited = false;//是否初始化
        $propMethodMap = [];//成员方法映射
        // 初始化func
        $initFunc = function(Message $repeatedObj) use(&$inited, &$propMethodMap): void {
            foreach(get_class_methods($repeatedObj) as $method) {
                if (str_starts_with($method, 'get')) {
                    $propMethodMap[self::getPropertyNameByMethodName($method)] = $method;
                }
            }
            $inited = true;
        };
        // 处理对象func
        $handleFunc = function(Message $repeatedObj, int $iterationKey) use($initFunc, &$inited, &$propMethodMap, $adjust, $remove, $rename) : array {
            if (! $inited) {
                $initFunc($repeatedObj);
            }
            $result = [];
            foreach ($propMethodMap as $property => $method) {
                // 根据映射关系调用方法赋值给成员
                $result[$property] = self::autoPropertyHandle($repeatedObj->{$method}());
            }
            // 移除的字段
            if ($remove) {
                foreach ($remove as $propertyName) {
                    if (isset($result[$propertyName])) unset($result[$propertyName]);
                }
            }
            // 字段值调整
            if ($adjust) {
                foreach ($adjust as $propertyName => $adjustValue) {
                    if (is_callable($adjustValue)) {
                        if (isset($result[$propertyName])) {
                            $result[$propertyName] = $adjustValue($result[$propertyName], $iterationKey);
                        } else {
                            $result[$propertyName] = $adjustValue(null, $iterationKey);
                        }
                    } else {
                        $result[$propertyName] = $adjustValue;
                    }
                }
            }
            // 重命名的字段
            if ($rename) {
                foreach ($rename as $oldName => $newName){
                    if (isset($result[$oldName])) {
                        $result[$newName] = $result[$oldName];
                        unset($result[$oldName]);
                    }
                }
            }
            return $result;
        };
        $result = [];
        foreach ($data as $key => $item) {
            if ($item instanceof Message) {
                $row = $handleFunc($item, $key);
            } else {
                $row = $item;
            }
            $result[] = $row;
        }
        return $result;
    }

    /**
     * @param Message|string|null   $message    需要处理的message
     * @param array|null            $adjust     字段值调整，一维关联数组，键为字段名，值为调整方法或调整值。如 ['create_time' => function($value){return date('Y-m-d', $value);}]
     * @param array|null            $remove     移除的字段，一维索引数组，值为要移除的字段名。如 ['create_time', 'update_time']
     * @param array|null            $rename     改名的字段，一维关联数组，键为字段名，值为新字段名。如 ['create_time' => 'created_datetime']
     *
     * @return array
     */
    public static function autoGenArray(Message|string|null $message, ?array $adjust = [], ?array $remove = [], ?array $rename = []): array {
        if (is_string($message) || is_null($message)) return [];
        $propMethodMap = [];//成员方法映射
        $methods = get_class_methods($message);
        foreach($methods as $method) {
            if (str_starts_with($method, 'get')) {
                $propMethodMap[self::getPropertyNameByMethodName($method)] = $method;
            }
        }
        $result = [];
        foreach ($propMethodMap as $property => $method) {
            // 根据映射关系调用方法赋值给成员
            $result[$property] = self::autoPropertyHandle($message->{$method}());
        }
        // 移除的字段
        if ($remove) {
            foreach ($remove as $propertyName) {
                if (isset($result[$propertyName])) unset($result[$propertyName]);
            }
        }
        // 字段值调整
        if ($adjust) {
            foreach ($adjust as $propertyName => $adjustValue) {
                if (is_callable($adjustValue)) {
                    if (isset($result[$propertyName])) {
                        $result[$propertyName] = $adjustValue($result[$propertyName]);
                    } else {
                        $result[$propertyName] = $adjustValue(null);
                    }
                } else {
                    $result[$propertyName] = $adjustValue;
                }
            }
        }
        // 重命名的字段
        if ($rename) {
            foreach ($rename as $oldName => $newName){
                if (isset($result[$oldName])) {
                    $result[$newName] = $result[$oldName];
                    unset($result[$oldName]);
                }
            }
        }
        return $result;
    }

    /**
     * @param MapField              $data       需要处理的关联数组或者MapField结构的数据
     * @param array|null            $adjust     字段值调整，一维关联数组，键为字段名，值为调整方法或调整值。如 ['create_time' => function($value){return date('Y-m-d', $value);}]
     * @param array|null            $remove     移除的字段，一维索引数组，值为要移除的字段名。如 ['create_time', 'update_time']
     * @param array|null            $rename     改名的字段，一维关联数组，键为字段名，值为新字段名。如 ['create_time' => 'created_datetime']
     *
     * @return array
     */
    public static function mapToAutoGenArray(MapField|array $data, array $adjust = [], array $remove = [], array $rename = []): array {
        $inited = false;//是否初始化
        $propMethodMap = [];//成员方法映射
        // 初始化func
        $initFunc = function(Message $message) use(&$inited, &$propMethodMap): void {
            foreach(get_class_methods($message) as $method) {
                if (str_starts_with($method, 'get')) {
                    $propMethodMap[self::getPropertyNameByMethodName($method)] = $method;
                }
            }
            $inited = true;
        };
        // 处理对象func
        $handleFunc = function(Message $message, $mapKey) use($initFunc, &$inited, &$propMethodMap, $adjust, $remove, $rename) : array {
            if (! $inited) {
                $initFunc($message);
            }
            $result = [];
            foreach ($propMethodMap as $property => $method) {
                // 根据映射关系调用方法赋值给成员
                $result[$property] = self::autoPropertyHandle($message->{$method}());
            }
            // 移除的字段
            if ($remove) {
                foreach ($remove as $propertyName) {
                    if (isset($result[$propertyName])) unset($result[$propertyName]);
                }
            }
            // 字段值调整
            if ($adjust) {
                foreach ($adjust as $propertyName => $adjustValue) {
                    if (is_callable($adjustValue)) {
                        if (isset($result[$propertyName])) {
                            $result[$propertyName] = $adjustValue($result[$propertyName], $mapKey);
                        } else {
                            $result[$propertyName] = $adjustValue(null, $mapKey);
                        }
                    } else {
                        $result[$propertyName] = $adjustValue;
                    }
                }
            }
            // 重命名的字段
            if ($rename) {
                foreach ($rename as $oldName => $newName){
                    if (isset($result[$oldName])) {
                        $result[$newName] = $result[$oldName];
                        unset($result[$oldName]);
                    }
                }
            }
            return $result;
        };
        $result = [];
        foreach ($data as $mapKey => $mapItem) {
            if ($mapItem instanceof Message) {
                $mapItem = $handleFunc($mapItem, $mapKey);
            }
            $result[$mapKey] = $mapItem;
        }
        return $result;
    }

    public static function repeatedToStringArray(\Google\Protobuf\Internal\RepeatedField $data): array {
        $result = [];
        if ($data->count() == 0) {
            return $result;
        }
        foreach ($data as $item) {
            $result[] = (string) $item;
        }
        return $result;
    }

    public static function repeatedToIntArray(\Google\Protobuf\Internal\RepeatedField $data): array {
        $result = [];
        if ($data->count() == 0) {
            return $result;
        }
        foreach ($data as $item) {
            $result[] = (int) $item;
        }
        return $result;
    }

    public static function foreachX($models, callable $callback, bool $checkIsEmpty = true) :array  {
        $list = [];
        if (empty($models)) {
            return $list;
        }
        if ($models instanceof Model\Collection && $models->count() == 0) {
            return $list;
        }
        foreach ($models as $model) {
            $item = $callback($model);
            if ($checkIsEmpty) {
                if (!empty($item)) {
                    $list[] = $item;
                }
            } else {
                $list[] = $item;
            }
        }
        return $list;
    }

    public static function boolToStringStatus(bool $val): string
    {
        if ($val) {
            return 'Y';
        }
        return 'N';
    }

    public static function stringStatusToBool(string $val): bool
    {
        if (0 === strcmp($val, 'Y')) {
            return true;
        }
        return false;
    }

    /**
     * 由Protobuf的方法名推算成员名称
     *
     * @param string $methodName
     *
     * @return string
     */
    private static function getPropertyNameByMethodName(string $methodName): string
    {
        $propertyName = $methodName;
        $len = strlen($propertyName);
        for ($i = 0; $i < $len; $i++)
        {
            $letter = $propertyName[$i];
            if (ord($letter) >= 65 && ord($letter) <= 90)
            {
                $propertyName = substr($propertyName, 0, $i). '_'. lcfirst(substr($propertyName, $i));
                $len++;
            }
        }
        $propertyName = substr($propertyName, 3);
        $propertyName = trim($propertyName, '_');
        $propertyName = strtolower($propertyName);
        return $propertyName;
    }

    private static function autoPropertyHandle(mixed $property): mixed
    {
        if ($property instanceof Message)
        {
            return self::autoGenArray($property);
        }
        elseif ($property instanceof RepeatedField)
        {
            return self::repeatedToAutoGenArray($property);
        }
        elseif ($property instanceof MapField)
        {
            return self::mapToAutoGenArray($property);
        }
        else
        {
            return $property;
        }
    }
}