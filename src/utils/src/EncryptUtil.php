<?php
declare(strict_types=1);

namespace Hyperfx\Utils;

class EncryptUtil {

    /**
     * 生成密码
     *
     */
    public static function generateMd5(string $password, string $salt = 'jufubao'): string
    {
        return md5(md5($password) . $salt);
    }

    /**
     * 生成sha256密码
     */
    public static function generateSha256(string $password, string $salt = 'jufubao'): bool|string
    {
        return hash('sha256', "password=".$password."&salt=".$salt);
    }

    /**
     * 生成签名
     */
    public static function makeSign(string $secret, array $data, int $expireTime): string {
        if ($expireTime < time()) {
            return '';
        }
        ksort($data);
        $signStr = '';
        foreach ($data as $k => $v) {
            $signStr .= $k . $v;
        }
        $signStr .= $secret;
        $sign   = sha1($signStr) . $expireTime;
        return md5($sign);
    }

    public static function makeSignV2(string $secret, array $data, int $time): string {
        ksort($data);
        $signStr = '';
        foreach ($data as $k => $v) {
            $signStr .= $k . $v;
        }
        $signStr .= $secret;
        $sign   = sha1($signStr) . $time;
        return md5($sign);
    }
}