<?php
declare(strict_types=1);

namespace Hyperfx\Utils;

use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\Utils\ApplicationContext;

class SessionCacheUtil
{

    private static function getCacheId() {
        $cacheId = ApplicationContext::getContainer()->get(RequestInterface::class)->cookie('JFB_SESSION_CID');
        if (empty($cacheId)) {
            $cacheId = ApplicationContext::getContainer()->get(RequestInterface::class)->header('x-jfb-session-cid');
        }
        if (!empty($cacheId)) {
            $cacheId = substr($cacheId, 0, 21);
        }
        return $cacheId;
    }
    
    public static function get(callable $hasCache, callable $noCache) {
        $cacheId = self::getCacheId();
        if (empty($cacheId)) {
            return $noCache();
        }
        return $hasCache($cacheId);
    }

    public static function call(callable $callback) {
        $cacheId = self::getCacheId();
        return $callback($cacheId);
    }
}