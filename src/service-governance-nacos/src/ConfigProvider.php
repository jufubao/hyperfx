<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\ServiceGovernanceNacos;

use Hyperf\Command\Listener\ClearTimerListener;
use Hyperfx\ServiceGovernanceNacos\Listener\CommandServiceClearListener;
use Hyperfx\ServiceGovernanceNacos\Listener\CommandServiceNodeListener;
use Hyperfx\ServiceGovernanceNacos\Listener\RegisterDriverListener;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'processes' => [
                // ServiceNodeSyncProcess::class,
            ],
            'dependencies' => [
                Client::class => ClientFactory::class,
            ],
            'listeners' => [
                // OnPipeMessageListener::class,
                RegisterDriverListener::class,
                CommandServiceNodeListener::class,
                CommandServiceClearListener::class,
                ClearTimerListener::class
            ],
        ];
    }
}
