<?php

namespace Hyperfx\ServiceGovernanceNacos;

use Hyperf\Contract\ConfigInterface;
use Hyperf\GrpcClient\Exception\GrpcClientException;
use Hyperf\ServiceGovernance\DriverInterface;
use Hyperfx\Framework\Constants\ErrorCode;
use Hyperfx\Framework\Logger\Logx;
use Hyperfx\ServiceGovernanceNacos\NodeSelector\NodeSelector;
use Hyperfx\ServiceGovernanceNacos\Util\ServiceUtil;
use Psr\Container\ContainerInterface;

class ServiceClient
{
    private array $configProviders = [];
    /**
     * @var DriverInterface
     */
    protected $driver;

    /**
     * @var $nodeSelector NodeSelector
     */
    private $nodeSelector;

    private int $lastClearPoolTime = 0;

    private array $servicePools = [];

    public function __construct(ContainerInterface $container)
    {
        $this->configProviders = $container->get(ConfigInterface::class)->get('services.providers', []);
        $this->driver = $container->get(\Hyperfx\ServiceGovernanceNacos\NacosDriver::class);
        $this->nodeSelector = $container->get(NodeSelector::class);
    }

    private function selectService($service): array {
        if (!isset($this->configProviders[$service])) {
            throw new GrpcClientException(sprintf('service [%s] not found', (string) $service), ErrorCode::SERVER_ERROR);
        }
        return $this->configProviders[$service];
    }

    private function getServicePoolKey($service, $hostname): string
    {
        return sprintf('%s@%s', $service, $hostname);
    }

    private function clearTimeoutPool(): void
    {
        $time = time();
        // 20秒内只请理一次
        if ($this->lastClearPoolTime + 20 > $time ) {
            return;
        }
        $this->lastClearPoolTime = $time;
        foreach ($this->servicePools as $key => $pool) {
            // 针对host来源，永不过期的
            if (!isset($pool['time'])) {
                continue;
            }
            // 5分种不用的清理掉
            if ($pool['time'] < $time - 60 * 5) {
                unset($this->servicePools[$key]);
            }
        }
    }

    public function create($service) {
        // 选择服务
        $currentService = $this->selectService($service);
        // 自定义host
        $hostname = ServiceUtil::findEnvHost($currentService['service']);
        if (!empty($hostname)) {
            return $this->discoverServerByHost($service, $currentService, $hostname);
        }
        return $this->discoverServerByNode($service, $currentService);
    }

    

    public function clear(): void
    {
        $this->servicePools = [];
    }

    private function discoverServerByHost($serviceClass, array $currentService, string $hostname) {
        $service = $currentService['service'];
        $options = $currentService['options'] ?? [];
        $key = $this->getServicePoolKey($service, $hostname);

        return make($serviceClass, [$hostname, $options]);
//        if (!isset($this->servicePools[$key])) {
//            $this->servicePools[$key] = [
//                'client' => make($serviceClass, [$hostname, $options]),
//            ];
//        }
//        return $this->servicePools[$key]['client'];
    }

    private function discoverServerByNode($serviceClass, array $currentService) {
        $service = $currentService['service'];
        $options = $currentService['options'] ?? [];

        // 通过nodeSelector选择服务
        $node = $this->nodeSelector->select($service);
        $hostname = sprintf('%s:%s', $node->host, $node->port);
        Logx::get()->debug('select service', [
            'service' => $service,
            'hostname' => $hostname,
        ]);

        return make($serviceClass, [$hostname, $options]);

        // 维护连接池
//        $key = $this->getServicePoolKey($service, $hostname);
//        if (!isset($this->servicePools[$key])) {
//            $this->servicePools[$key] = [
//                'client' => make($serviceClass, [$hostname, $options]),
//                'time' => time(),
//            ];
//        } else {
//            $this->servicePools[$key]['time'] = time();
//        }
//        $this->clearTimeoutPool();
//        return $this->servicePools[$key]['client'];
    }

}