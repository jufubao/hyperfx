<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\ServiceGovernanceNacos\NodeSelector;

use Hyperf\Utils\ApplicationContext;
use Hyperf\Grpc\StatusCode;
use Hyperf\GrpcClient\Exception\GrpcClientException;
use Hyperf\LoadBalancer\AbstractLoadBalancer;
use Hyperf\LoadBalancer\LoadBalancerManager;
use Hyperf\LoadBalancer\Node;
use Hyperfx\Framework\Logger\Logx;

class NodeSelector
{
    /**
     * @var array
     */
    public $services;

    private string $algorithm = 'random';

    public function register(string $service, array $nodes): void
    {
        $loadBalancerManager = new LoadBalancerManager();
        $loadBalancer = $loadBalancerManager->get($this->algorithm);
        $this->services[$service] = make($loadBalancer, [$nodes]);
    }

    public function has(string $service): bool {
        return isset($this->services[$service]);
    }

    public function refresh(string $service, array $nodes): void
    {
        if (empty($nodes)) {
            return;
        }
        /* @var $loadBalancer AbstractLoadBalancer */
        $loadBalancer = $this->services[$service];
        $loadBalancer->setNodes($nodes);
    }

    public function select(string $service, array ...$parameters): Node
    {
        if (!isset($this->services[$service])) {
            // 从Nacos里获取
            $driverManager = ApplicationContext::getContainer()->get(\Hyperf\ServiceGovernance\DriverManager::class);
            $arrNodes = $driverManager->get('nacos')->getNodes('', $service, []);
            if (empty($arrNodes)) {
                throw new GrpcClientException(sprintf('此服务[%s]尚未注册,请检查配置文件 %s', $service, posix_getpid()), StatusCode::INTERNAL);
            }
            $nodes = [];
            foreach ($arrNodes as $arrNode) {
                $nodes[] = new Node($arrNode['host'], $arrNode['port'], $arrNode['weight']);
            }
            $this->register($service, $nodes);
            Logx::get()->error('未开启服务发现，请开启服务发现，如确认已经开启请忽略。注：频繁提示代表没有开启');
        }
        /* @var $loadBalancer AbstractLoadBalancer */
        $loadBalancer = $this->services[$service];
        return $loadBalancer->select($parameters);
    }
}
