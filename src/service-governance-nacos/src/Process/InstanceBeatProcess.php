<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\ServiceGovernanceNacos\Process;

use Hyperf\Contract\ConfigInterface;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Coordinator\Constants;
use Hyperf\Coordinator\CoordinatorManager;
use Hyperf\Nacos\Application;
use Hyperf\Process\AbstractProcess;
use Hyperf\Process\ProcessManager;
use Hyperf\Contract\IPReaderInterface;
use Hyperfx\Utils\AppEnvUtil;
use Hyperfx\Utils\NetworkUtil;

class InstanceBeatProcess extends AbstractProcess
{
    /**
     * @var string
     */
    public string $name = 'nacos-heartbeat';

    protected int $restartInterval = 1;

    public function handle(): void
    {
        $config = $this->container->get(ConfigInterface::class);
        $logger = $this->container->get(StdoutLoggerInterface::class);
        $client = $this->container->get(Application::class);

        $serviceConfig = $config->get('services.drivers.nacos', []);
        $serviceName = $serviceConfig['service_name'];
        $namespaceId = $serviceConfig['namespace_id'];
        $groupName = $serviceConfig['group_name'] ?? null;
        $instanceConfig = $serviceConfig['instance'] ?? [];
        $ephemeral = $instanceConfig['ephemeral'] ?? null;
        $cluster = $instanceConfig['cluster'] ?? null;
        $weight = $instanceConfig['weight'] ?? null;
        $ip = $this->getIp();

        $maxLoop = 100;
        while (ProcessManager::isRunning()) {
            $heartbeat = $config->get('services.drivers.nacos.instance.heartbeat', 5);
            sleep($heartbeat ?: 5);

            $ports = $config->get('server.servers', []);
            foreach ($ports as $portServer) {
                $name = $portServer['name'] ?? '';
                if ($name !== 'grpc') {
                    continue;
                }
                $port = (int) $portServer['port'];
                $response = $client->instance->beat(
                    $serviceName,
                    [
                        'ip' => $ip,
                        'port' => $port,
                        'serviceName' => $groupName . '@@' . $serviceName,
                        'cluster' => $cluster,
                        'weight' => $weight,
                    ],
                    $groupName,
                    $namespaceId,
                    $ephemeral
                );
                if ($response->getStatusCode() === 200) {
                    $logger->debug(sprintf('Instance %s:%d heartbeat successfully!', $ip, $port));
                } else {
                    $logger->error(sprintf('Instance %s:%d heartbeat failed! error: %s', $ip, $port, $response->getBody()->getContents()));
                }
            }
            $maxLoop--;
            if ($maxLoop <= 0) {
                break;
            }
        }
    }

    private function getIp() {
        $xhostName = env("X-HOSTNAME");
        if (!empty($xhostName)) {
            return $xhostName;
        }

        $ips = [];
        if (function_exists('swoole_get_local_ip')) {
            $ips = swoole_get_local_ip();
        }
        if (empty($ips) && function_exists('net_get_interfaces')) {
            foreach (net_get_interfaces() ?: [] as $name => $value) {
                foreach ($value['unicast'] as $item) {
                    if (! isset($item['address'])) {
                        continue;
                    }
                    if (! Str::contains($item['address'], ':') && $item['address'] !== '127.0.0.1') {
                        $ips[$name] = $item['address'];
                    }
                }
            }
        }
        if (is_array($ips) && ! empty($ips)) {
            // 开发环境,优先使用10.网段IP
            if (AppEnvUtil::isDev()) {
                foreach ($ips as $ip) {
                    if (str_starts_with($ip, '10.')) {
                        return $ip;
                    }
                }
            }
            return current($ips);
        }

        $name = gethostname();
        if ($name === false) {
            throw new RuntimeException('Can not get the internal IP.');
        }

        return gethostbyname($name);
    }
    public function isEnable($server): bool
    {
        $config = $this->container->get(ConfigInterface::class);
        return $config->get('services.enable.register', true) && $config->get('services.drivers.nacos.instance.heartbeat', 0);
    }
}
