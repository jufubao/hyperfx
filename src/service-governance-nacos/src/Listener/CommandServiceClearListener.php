<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\ServiceGovernanceNacos\Listener;

use Hyperf\Command\Event\AfterHandle;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperfx\ServiceGovernanceNacos\ServiceClient;
use Psr\Container\ContainerInterface;

class CommandServiceClearListener implements ListenerInterface
{

    public function __construct(protected ContainerInterface $container)
    {

    }

    public function listen(): array
    {
        return [
            AfterHandle::class,
        ];
    }

    public function process(object $event): void
    {
        $client = $this->container->get(ServiceClient::class);
        $client->clear();
    }
}
