<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\ServiceGovernanceNacos\Listener;
use Hyperf\Process\ProcessManager;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Coordinator\Constants;
use Hyperf\Coordinator\CoordinatorManager;
use Hyperf\Server\ServerFactory;
use Hyperf\Utils\Codec\Json;
use Hyperfx\Framework\Logger\Logx;
use Hyperfx\ServiceGovernanceNacos\Contract\PipeMessageInterface;
use Hyperf\Framework\Event\OnPipeMessage;
use Hyperf\LoadBalancer\Node;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Process\Event\PipeMessage as UserProcessPipeMessage;
use Hyperf\ServiceGovernance\DriverManager;
use Hyperfx\ServiceGovernanceNacos\NodeSelector\NodeSelector;
use Hyperfx\Utils\AppEnvUtil;
use Psr\Container\ContainerInterface;

class OnPipeMessageListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var StdoutLoggerInterface
     */
    protected $logger;

    /**
     * @var DriverManager
     */
    protected $driverManager;

    private static int $maxLoop = 1000;
    private static int $initLoop = 1000;

    public function __construct(DriverManager $manager, ContainerInterface $container)
    {
        $this->logger = $container->get(StdoutLoggerInterface::class);
        $this->nodeSelector = $container->get(NodeSelector::class);
    }

    public function listen(): array
    {
        return [
            OnPipeMessage::class,
            UserProcessPipeMessage::class,
        ];
    }

    public function process(object $event): void
    {
        if ($event instanceof OnPipeMessage || $event instanceof UserProcessPipeMessage) {
            $event->data instanceof PipeMessageInterface && $this->onPipeMessage($event->data);
        }
    }
    public function onPipeMessage($message) {
        $data = $message->getData();
        $service = $data[0];
        $items = $data[1];

        $nodes = [];
        foreach ($items as $item) {
            $nodes[] = new Node($item['host'], $item['port'], $item['weight']);
        }

        if (!$this->nodeSelector->has($service)) {
            $this->nodeSelector->register($service, $nodes);
        } else {
            $this->nodeSelector->refresh($service, $nodes);
        }

        $this->logger->debug(sprintf('Process onPipeMessage service node refresh successful'), [
            'service' => $service,
            'nodes' => Json::encode($data)
        ]);

        // 仅限非生产模式下，用于测试，模拟服务重启
        if (AppEnvUtil::isProd()) {
            return;
        }

        self::$maxLoop--;
        if (self::$maxLoop < 0) {
            self::$maxLoop = self::$initLoop;

            CoordinatorManager::until(Constants::WORKER_START)->resume();
            CoordinatorManager::until(Constants::WORKER_EXIT)->resume();
            ProcessManager::setRunning(false);

            Logx::get()->info(sprintf('为了释放内存，停止了当前进程pid:%u，仅限测试环境', getmypid()));

            // worker下停止work
            $httpServer = ApplicationContext::getContainer()->get(ServerFactory::class)->getServer()->getServer();
            $workId = $httpServer->getWorkerId();
            if (is_int($workId)) {
                $httpServer->stop($workId, true);
            }
        }
    }
}
