<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\ServiceGovernanceNacos\Listener;

use Hyperf\Command\Event\BeforeHandle;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Utils\ApplicationContext;
use Hyperf\Utils\Codec\Json;
use Hyperf\Utils\Parallel;
use Hyperfx\Framework\Logger\Logx;
use Hyperf\Framework\Event\OnPipeMessage;
use Hyperf\LoadBalancer\Node;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\ServiceGovernance\DriverManager;
use Hyperfx\ServiceGovernanceNacos\NodeSelector\NodeSelector;
use Hyperfx\ServiceGovernanceNacos\Util\ServiceUtil;
use Psr\Container\ContainerInterface;
use Swoole\Timer;

class CommandServiceNodeListener implements ListenerInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var StdoutLoggerInterface
     */
    protected $logger;

    /**
     * @var DriverManager
     */
    protected $driverManager;

    /**
     * @var ConfigInterface
     */
    private $config;

    public function __construct(DriverManager $manager, ContainerInterface $container)
    {
        $this->logger = $container->get(StdoutLoggerInterface::class);
        $this->config = $container->get(ConfigInterface::class);
        $this->nodeSelector = $container->get(NodeSelector::class);
    }

    public function listen(): array
    {
        return [
            BeforeHandle::class,
        ];
    }

    public function process(object $event): void
    {
        if (!$this->isEnable()) {
            return;
        }
        $providers = $this->config->get('services.providers');

        $container = ApplicationContext::getContainer();
        $nacosDriver = $container->get(DriverManager::class)->get("nacos");
        $nodeSelector = $container->get(NodeSelector::class);

        // 保证最小值为3秒
        $interval = (int) $this->config->get('services.drivers.nacos.discovery_interval', 3);
        if ($interval < 3) {
            $interval = 3;
        }

        // 先执行一次
        $this->discoveryProviders($providers, $nacosDriver, $nodeSelector);

        // 定时触发
        Timer::tick($interval * 1000, function () use ($providers, $nacosDriver, $nodeSelector) {
            $this->discoveryProviders($providers, $nacosDriver, $nodeSelector);
        });
    }

    

    private function isEnable(): bool {
        return $this->config->get('services.enable.discovery', true)
            && !empty($this->config->get('services.providers', []))
            && !empty($this->config->get('services.drivers.nacos', []))
            && ServiceUtil::checkIsOpenDiscovery();
    }

    private function discoveryProviders(array $providers, $nacosDriver, $nodeSelector) {
        $parallel = new Parallel();
        foreach ($providers as $provider) {
            $host = ServiceUtil::findEnvHost($provider['service']);
            if (!empty($host)) {
                continue;
            }
            $parallel->add(function () use ($provider, $nacosDriver, $nodeSelector) {
                $this->discoveryOneProvider($provider, $nacosDriver, $nodeSelector);
            });
        }
        $parallel->wait();
    }
    private function discoveryOneProvider($provider, $nacosDriver, $nodeSelector) {
        $service = $provider['service'];
        $metadata = $provider['metadata'] ?? [];
        $data = $nacosDriver->getNodes('', $service, []);
        if (empty($data)) {
            Logx::get()->alert('No nodes available', [
                'service' => $service,
                'metadata' => Json::encode($metadata),
            ]);
            return;
        }
        $nodes = [];
        foreach ($data as $item) {
            $nodes[] = new Node($item['host'], $item['port'], $item['weight']);
        }
        if (!$nodeSelector->has($service)) {
            $nodeSelector->register($service, $nodes);
        } else {
            $nodeSelector->refresh($service, $nodes);
        }
    }
}
