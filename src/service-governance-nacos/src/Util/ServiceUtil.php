<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace Hyperfx\ServiceGovernanceNacos\Util;


use Hyperfx\Utils\AppEnvUtil;

class ServiceUtil
{
    public static function findEnvHost(string $serviceName): string {
        static $map = [];
        if (isset($map[$serviceName])) {
            $serviceName = $map[$serviceName];
        } else {
            $serviceName = $map[$serviceName] = strtoupper(str_replace('.', '_', $serviceName));
        }

        // sandbox
        $sandboxHostName = sprintf('GRPC_%s_HOST', $serviceName);
        $sandboxHostValue = env($sandboxHostName, '');
        if (!empty($sandboxHostValue)) {
            return $sandboxHostValue;
        }

        // ali
        $aliHostName = sprintf('%s_SVC_SERVICE_HOST', $serviceName);
        $aliHostValue = env($aliHostName, env(sprintf('%s_SERVICE_HOST', $serviceName), ''));
        if (empty($aliHostValue)) {
            return self::defaultSvcHost($serviceName);
        }
        $aliPortName = sprintf('%s_SVC_SERVICE_PORT', $serviceName);
        $aliPortValue = env($aliPortName, env(sprintf('%s_SERVICE_PORT', $serviceName), ''));
        if (empty($aliPortValue)) {
            return self::defaultSvcHost($serviceName);
        }

        return sprintf('%s:%s', $aliHostValue, $aliPortValue);
    }

    private static function defaultSvcHost($serviceName) {
        if (AppEnvUtil::isProd()) {
            $host = str_replace('.', '-', $serviceName);
            return sprintf('%s-svc:9503', $host);
        }
        return '';
    }

    /**
     * 检测是否开启服务发现
     */
    public static function checkIsOpenDiscovery(): bool {
        $providers = config('services.providers', []);
        foreach ($providers as $provider) {
            $host = ServiceUtil::findEnvHost($provider['service']);
            if (empty($host)) {
                return true;
            }
        }
        return false;
    }
}
