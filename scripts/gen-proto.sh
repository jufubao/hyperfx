#!/bin/bash

mkdir -p grpc/Grpc

protoc --proto_path=. --php_out=./grpc --grpc_out=./grpc --plugin=protoc-gen-grpc=/opt/grpc/bin/grpc_php_plugin $1

echo '<?php
declare(strict_types=1);

namespace Grpc;

use Hyperf\GrpcClient\BaseClient;
use Hyperf\Utils\ApplicationContext;
use Hyperfx\ServiceGovernanceNacos\ServiceClient;

class BaseStub extends BaseClient
{
   /**
     * @return parent
     */
    public static function create()  {
        $container = ApplicationContext::getContainer();
        $client = $container->get(ServiceClient::class);
        return $client->create(get_called_class());
    }
}' > ./grpc/Grpc/BaseStub.php